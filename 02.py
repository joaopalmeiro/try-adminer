import json

from ruyaml import YAML

OUTPUT_FILE = "compose.yaml"
PORT = 8080
IMAGE = "adminer"
THEME_VAR = "ADMINER_DESIGN"

if __name__ == "__main__":
    # https://github.com/pycontribs/ruyaml/blob/v0.91.0/lib/ruyaml/main.py#L61
    yaml = YAML(typ="rt", pure=False)

    with open("themes.json", "r", encoding="utf-8") as f:
        themes = json.load(f)
    # print(themes)

    services = {}
    theme_port = PORT
    for theme_name in themes["names"]:
        # https://docs.docker.com/compose/compose-file/05-services/#environment
        services[theme_name] = {
            "container_name": theme_name,
            "image": IMAGE,
            "ports": [f"{theme_port}:{PORT}"],
            "environment": {THEME_VAR: theme_name},
        }
        theme_port += 1

    # https://ruyaml.readthedocs.io/en/latest/basicuse.html#basic-usage
    # https://github.com/pycontribs/ruyaml/blob/v0.91.0/lib/ruyaml/main.py#L578
    with open(OUTPUT_FILE, "w", encoding="utf-8") as f:
        yaml.dump({"name": "adminer-themes", "version": "3.8", "services": services}, f)

    print("Done! ✓")
