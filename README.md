# try-adminer

## Development

```bash
cp .env.example .env
```

```bash
pyenv install && pyenv versions
```

```bash
pip install pipenv && pipenv --version
```

```bash
pipenv install --dev --skip-lock
```

```bash
pipenv run pre-commit install
```

```bash
pipenv run ruff check --verbose .
```

```bash
pipenv run ruff check --fix --verbose .
```

```bash
pipenv run ruff format --verbose .
```

```bash
pipenv run python 01.py
```

```bash
pipenv run python 02.py
```

```bash
docker compose -f compose.yaml config --quiet
```

```bash
docker compose -f compose.yaml up --detach --no-recreate
```

```bash
docker compose -f compose.yaml down
```
