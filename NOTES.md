# Notes

- https://hub.docker.com/_/adminer/
- https://github.com/TimWolla/docker-adminer
- https://github.com/vrana/adminer/tree/master/designs
- https://github.com/adminerevo/adminerevo
- https://github.com/PyGithub/PyGithub
- https://docs.astral.sh/ruff/settings/#line-length
- https://docs.astral.sh/ruff/formatter/#conflicting-lint-rules
- https://docs.astral.sh/ruff/formatter/black/
- https://github.com/astral-sh/ruff/issues/4464
- https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html
- https://docs.astral.sh/ruff/rules/#isort-i
- https://docs.astral.sh/ruff/settings/#isort-combine-as-imports
- [Async support](https://github.com/PyGithub/PyGithub/issues/1538) (open) issue
- [Is the PyGithub project dead? How can the community help?](https://github.com/PyGithub/PyGithub/issues/2178) (open) issue
- https://docs.github.com/en/rest/repos/contents?apiVersion=2022-11-28:
  - "When listing the contents of a directory, submodules have their "type" specified as "file". Logically, the value should be "submodule". This behavior exists in API v3 for backwards compatibility purposes. In the next major version of the API, the type will be returned as "submodule"."
  - "If the content is a submodule: The `submodule_git_url` [(`git_url`)] identifies the location of the submodule repository (...)"
- https://pypi.org/project/oitnb/ (alternative to Black)
- https://github.com/pycontribs/ruyaml
- https://ruyaml.readthedocs.io/en/latest/
- https://docs.docker.com/compose/compose-file/compose-versioning/
- https://docs.docker.com/compose/compose-file/compose-versioning/#version-38
- https://docs.docker.com/engine/reference/commandline/compose_up/
- https://docs.docker.com/compose/compose-file/05-services/#restart
- https://www.adminer.org/
- https://www.npmjs.com/package/@secretlint/secretlint-rule-github
- https://github.com/pre-commit/pre-commit-hooks/blob/main/pre_commit_hooks/detect_private_key.py
- https://github.com/gitleaks/gitleaks
- https://github.com/gitleaks/gitleaks#pre-commit

## Commands

```bash
pipenv --python $(cat .python-version)
```

```bash
pipenv install --skip-lock PyGithub==2.1.1 python-dotenv==1.0.0 ruyaml==0.91.0 && pipenv install --dev --skip-lock ruff==0.1.5 pre-commit==3.5.0
```

```bash
pipenv run ruff check --help
```

### List all services

```bash
docker compose -f compose.yaml config --services
```
