import json
import os

from dotenv import load_dotenv
from github import Auth, Github

ADMINER_VERSION = "v4.8.1"

if __name__ == "__main__":
    load_dotenv()

    # https://pygithub.readthedocs.io/en/stable/introduction.html#very-short-tutorial
    # https://github.com/PyGithub/PyGithub#simple-demo
    auth = Auth.Token(os.getenv("GITHUB_TOKEN"))

    # https://pygithub.readthedocs.io/en/stable/github.html#github.MainClass.Github.close (alternative to `g.close()`)
    with Github(auth=auth) as g:
        # https://pygithub.readthedocs.io/en/stable/github.html
        # print(type(g))
        repo = g.get_repo("vrana/adminer")

        # https://pygithub.readthedocs.io/en/stable/github_objects/Repository.html
        # https://pygithub.readthedocs.io/en/stable/github_objects/ContentFile.html
        # https://github.com/PyGithub/PyGithub/blob/v2.1.1/github/ContentFile.py#L45
        folder = repo.get_contents("designs", ref=ADMINER_VERSION)
        # print(folder)
        # print(type(folder[0]))
        # print(folder[0].name)
        # print(folder[0].type)
        # print(folder[0].git_url)

    # https://stackoverflow.com/a/53889814
    # https://docs.python.org/3.10/library/stdtypes.html#str.startswith
    themes = [
        file.name for file in folder if not file.name.startswith(("readme", "README"))
    ]
    # print(themes)
    print(f"{len(themes)} themes")

    output = {"version": ADMINER_VERSION, "names": themes}

    with open("themes.json", "w", encoding="utf-8") as f:
        # https://docs.python.org/3.10/library/json.html
        # json.dump(output, f, ensure_ascii=False, indent=4)
        json.dump(output, f, ensure_ascii=False, separators=(",", ":"))

    print("Done! ✓")
